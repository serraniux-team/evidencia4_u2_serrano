
let posiciones=[45,43,67,2,21,56,32,12,34,56,89,96,54,46,67,32,77,88,54,67];

console.log("El promedio es: "+promedio(posiciones));
console.log("La cantidad de pares es:"+pares(posiciones));
console.log("Ordenados de mayor a menor: "+menormayor(posiciones));

function promedio(posiciones){
    var element; 
    for (let i = 0; i < posiciones.length; i++) {
        element =+ posiciones[i];
        
    }
    element=element/20;
    return element;
}

function pares(posiciones){
    let paress=0;
    for(let i= 0;i<posiciones.length;i++){
        if((posiciones[i] % 2) ==0){
            paress++;
        }
    }
    return paress;
}

function mayormenor(posiciones){
    return posiciones.sort(function(a, b){return b - a});
}
/*
El método sort() puede ordenar valores negativos, cero y positivos en el orden correcto. Cuando compara dos valores, los envía a nuestra función de comparación y luego ordena los valores de acuerdo al resultado devuelto.
*/
function menormayor(posiciones){
    return posiciones.sort(function(a, b){return a-b});
}

function Numeros(string){//Solo numeros
    var out = '';
    var filtro = '1234567890';//Caracteres validos
	
    //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
    for (var i=0; i<string.length; i++)
       if (filtro.indexOf(string.charAt(i)) != -1) 
             //Se añaden a la salida los caracteres validos
	     out += string.charAt(i);
	
    //Retornar valor filtrado
    return out;
} 
function borrar() {
    document.getElementById("numeros").innerHTML= "";
  }
function generacion(){
    
    borrar();
    var limite = document.getElementById('limite').value;
    var numeros = document.getElementById("numeros");
    if (limite<1 || limite>50) {
        alert("Rango no permitido");
    }
    else{

        var lista=new Array();
        lista.length=limite;
        for (let i = 0; i < lista.length; i++) {
            lista[i] = i+1;
            
        }
        for (let i = 0; i < lista.length; i++) {
            var x=numerorandom(1,50);
            if (lista.indexOf(x)==-1) {
                lista[i] = x;
            } 
            
            
        }
        lista=menormayor(lista);
        for (let con = 0; con < limite; con++) {
            
        
            numeros.options[con]=new Option(lista[con]);
            
        }

    }
    paresImp(lista);
    
    
}
function aleatorio(){
    borrar();
    var limite = document.getElementById('limite').value;
    var numeros = document.getElementById("numeros");
    if (limite<1 || limite>50) {
        alert("Rango no permitido");
    }
    else{

        var lista=new Array();
        lista.length=limite;
        for (let i = 0; i < lista.length; i++) {
            lista[i] = i+1;
            
        }
        for (let i = 0; i < lista.length; i++) {
            var x=numerorandom(1,50);
            /*
            El método indexOf() devuelve el primer índice encontrado de un elemento específico. Devuelve -1 si el elemento no existe en el arreglo
            */
            if (lista.indexOf(x)==-1) {
                lista[i] = x;
            } 
            
            
        }
        if (limite>45) {
            lista = lista.sort(function() {return Math.random() - 0.5});
        }
        
        
        for (let con = 0; con < limite; con++) {
            
        
            numeros.options[con]=new Option(lista[con]);
            
        }
        paresImp(lista);

    }
}
/*
Tenemos el método numerorandom() para generar un número aleatorio dentro de un cierto rango. 
*/
function numerorandom(min, max) {
    let paso1 = max - min + 1;
    /*
    Math.random() devuelve un número aleatorio entre 0 (inclusivo), y 1 (exclusivo):
    */
    let paso2 = Math.random() * paso1;
    /*
    Math.floor(x) devuelve el valor de x redondeado a su número entero más cercano:
    */
    let r = Math.floor(paso2) + min;
    return r;
}
function MayMen(){
    
    borrar();
    var limite = document.getElementById('limite').value;
    var numeros = document.getElementById("numeros");
    if (limite<1 || limite>50) {
        alert("Rango no permitido");
    }
    else{

        var lista=new Array();
        lista.length=limite;
        for (let i = 0; i < lista.length; i++) {
            lista[i] = i+1;
            
        }
        for (let i = 0; i < lista.length; i++) {
            var x=numerorandom(1,50);
            if (lista.indexOf(x)==-1) {
                lista[i] = x;
            } 
            
            
        }
        
        lista=mayormenor(lista);
        for (let con = 0; con < limite; con++) {
            
        
            numeros.options[con]=new Option(lista[con]);
            
        }

    }
    paresImp(lista);
    
    
}

/**
 Contar los numeros pares e impares para saber su porcentaje la diferencia no sea mayor a 25%
 */

 function paresImp(posiciones){
    let paress=0;
    let imparess=0;
    var porPar=0;
    var porImp=0;
    var dif=0;
    for(let i= 0;i<posiciones.length;i++){
        if((posiciones[i] % 2) ==0){
            paress++;
        }
        else{
            imparess++;
        }
    }
    
    porPar=((paress*100)/posiciones.length).toFixed(0);
    porImp=((imparess*100)/posiciones.length).toFixed(0);
    document.getElementById("PorPares").innerHTML=porPar+"%";
    document.getElementById("PorImpares").innerHTML=porImp+"%";
    var dif=(porPar-porImp);
    if (dif<0) {
        dif=dif*-1;
    }
    if (dif>25) {
        document.getElementById("EsSimetrico").innerHTML="NO ES SIMETRICO";
    }
    else{
        document.getElementById("EsSimetrico").innerHTML="SI ES SIMETRICO";
    }

    
}

